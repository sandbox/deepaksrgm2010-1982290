/**
* @file Add line item when additional donation is changed
*/

$(document).ready(
  function() {
    // Hide the contents.
    $('#edit-panes-uc-donation-incentives-donation-additional').change(function () {
      var donation_additional = $("input[@name*=donation_additional]").val();
      set_line_item("donation_additional", "Extra Donation", Number(donation_additional).toFixed(2), 1);
    });
  }
);
// 9597684616
// 8650n
// 011204055