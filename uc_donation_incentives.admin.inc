<?php

/**
 * @file uc_donation_incentive done by s.deepak 011204055 
 */

/**
 * uc donation incentives admin setting form
 * @return type-fapi array
 */
function uc_donation_incentives_admin() {
  $output = drupal_get_form('uc_donation_incentives_default_settings');
  $output += drupal_get_form('uc_donation_incentives_settings');
  return $output;
}

/**
 * One of the admin forms displayes in donation incentive admin setting
 * @param type $form_state
 * @return array - fapi array
 */
function uc_donation_incentives_default_settings($form_state) {
  $default = uc_donation_incentives_global_match_default();
  $form = array();
  $form['donation'] = array(
    '#type' => 'fieldset',
    '#title' => t('Donation Incentive Defaults'),
    '#description' => t('These defaults will be applied to all products, and are override-able at the the catalog and product level.'),
    '#tree' => TRUE,
  );
  $form['donation']['match_type'] = array(
    '#type' => 'select',
    '#title' => t('Donation incentive type'),
    '#description' => t('How would you like to determine the donation incentive a product receives?'),
    '#options' => array(
      'none' => 'no donation',
      'fixed' => 'fixed amount',
      'sell_fraction' => 'percentage of product sell price',
    ),
    '#default_value' => $default->match_type,
  );
  $form['donation']['match_rate'] = array(
    '#type' => 'textfield',
    '#title' => t('Donation incentive rate'),
    '#description' => t('The rate at which a donation is given. For "fixed amount", enter a penny amount (ie: for $1 enter 100). For "percentage" options, enter a value from 0 to 100.'),
    '#default_value' => $default->match_rate,
  );
  $form['donation']['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Save',
  );
  $form['#validate'][] = 'uc_donation_incentives_match_validate';
  return $form;
}

/**
 * Another form displayed donation incentives admin pages
 * @param type $form
 * @param type $form_state
 */
function uc_donation_incentives_default_settings_submit(&$form, &$form_state) {
  $target_type = 'default';
  $match_type = $form_state['values']['donation']['match_type'];
  $match_rate = $form_state['values']['donation']['match_rate'];
  uc_donation_incentives_settings_save($match_type, $match_rate, $target_type);
  drupal_set_message(t('The settings have been saved.'));
}

/**
 * Another form displayed donation incentives admin pages
 * @return array
 */
function uc_donation_incentives_settings() {
  $form['donation_display'] = array(
    '#type' => 'fieldset',
    '#title' => t('Display Settings'),
    '#description' => t('These settings control how donation incentives appear on your site.'),
  );
  $form['donation_display']['uc_donation_incentives_cause'] = array(
    '#type' => 'textfield',
    '#title' => t('Destination for Donations'),
    '#description' => t('Used for describing where donations go. For example: "Donations will be made to <u>your <b>Cause of Choice</b></u>."'),
    '#default_value' => variable_get('uc_donation_incentives_cause', 'your <b>Cause of Choice</b>'),
  );
  $form['donation_display']['uc_donation_incentives_icon_small'] = array(
    '#type' => 'textfield',
    '#title' => t('Big Donation Indicator Icon Path'),
    '#description' => t('Path to image file. Leave blank to use default 22x22 icon'),
    '#default_value' => variable_get('uc_donation_incentives_icon_small', drupal_get_path('module', 'uc_donation_incentives') . '/heart22x22.png'),
  );
  $form['donation_display']['uc_donation_incentives_icon_big'] = array(
    '#type' => 'textfield',
    '#title' => t('Small Donation Indicator Icon Path'),
    '#description' => t('Path to image file. Leave blank to use default 32x32 icon'),
    '#default_value' => variable_get('uc_donation_incentives_icon_big', drupal_get_path('module', 'uc_donation_incentives') . '/heart32x32.png'),
  );
  $form['donation_display']['uc_donation_incentives_description'] = array(
    '#type' => 'textarea',
    '#title' => t('Donation Description'),
    '#description' => t('Displayed on cart preview and checkout panes'),
    '#default_value' => variable_get('uc_donation_incentives_description', 'After you complete the checkout process, you will have the opportunity to select the causes you wish to support.'),
  );
  return system_settings_form($form);
}

/**
 * Donation incentives admin reports page
 * @return array
 */
function uc_donation_incentives_report() {
  if (arg(4)) {
    $status = arg(4);
  }
  else {
    $status = '';
  }
  $results = uc_donation_incentives_report_get($status);
  $data = array();
  while ($row = $results->fetchField()) {
    $order = uc_order_load($row->order_id);
    if ($order->order_status == 'payment_received' || $order->order_status == 'complete')
      $data[] = array(l($row->order_id, 'admin/store/orders/' . $row->order_id), 
        check_plain(uc_currency_format($row->rebated)),
        check_plain(uc_currency_format($row->purchased)), 
        $row->status,
        format_date($row->added_timestamp,'custom', variable_get('uc_date_format_default', 'm/d/Y')),
        );
  }
  $header = array(
    t('Order ID'), 
    t('Rebated'),
    t('Purchased'), 
    t('Status'), 
    t('Created'),
    );
  $header = array(
    'order_id' => array('data' => 'Order ID', 'field' => 'u.order_id',),
    'rebated' => array('data' => t('Rebated'), 'field' => 'u.rebated',),
    'purchased' => array('data' => t('Purchased'),),
    'status' => array('data' => t('Status'), 'field' => 'u.status', 'sort' => 'desc',),
  );
  $query = db_select('uc_donation_incentives_orders', 'u');
  if ($status) {
    $query->condition('u.status', $status, '=');
  }
  $count_query = clone $query;
  $count_query->addExpression('COUNT(u.order_id)');
  $query = $query->extend('PagerDefault')->extend('TableSort');
  $query
      ->fields('u', array('id', 'order_id', 'rebated', 'purchased', 'status',))
      ->limit(10)
      ->orderByHeader($header)
      ->setCountQuery($count_query);
  $result = $query->execute();
  $options = array();
  $destination = drupal_get_destination();
  $status = array(t('blocked'), t('active'),);
  $roles = array_map('check_plain', user_roles(TRUE));
  $accounts = array();
  foreach ($result as $account) {
    $users_roles = array();
    $roles_result = db_query('SELECT rid FROM {users_roles} WHERE uid = :uid', array(':uid' => $account->uid,));
    foreach ($roles_result as $user_role) {
      $users_roles[] = $roles[$user_role->rid];
    }
    asort($users_roles);
    $options[$account->id] = array(
      'order_id' => $account->order_id,
      'rebated' => $account->rebated,
      'purchased' => $account->purchased,
      'status' => $account->status,
   );
  }
  $form['accounts'] = array(
    '#theme' => 'table',
    '#tree' => TRUE,
    '#header' => $header,
    '#rows' => $options,
    '#empty' => t('No Order Placed'),
  );
  $form['pager'] = array('#markup' => theme('pager'),);
  $form['#attributes'] = array();
  return $form;
}

/**
 * API to get report of donation generated by each order
 * @param type $status
 * @return object or null - 
 */
function uc_donation_incentives_report_get($status) {
  if ($status) {
    $sql = 'SELECT * FROM {uc_donation_incentives_orders} WHERE status = :status AND deleted_timestamp = 0 ORDER BY added_timestamp DESC';
  }
  else {
    $sql = 'SELECT * FROM {uc_donation_incentives_orders} WHERE deleted_timestamp = 0 ORDER BY added_timestamp DESC';
  }
  $results = db_query($sql, array('status' => $status,));
  return $results;
}

