<?php
/**
 * @file uc_donation_incentive done by s.deepak 011204055
 */

/**
 * Handle the Payment order pane.
 */
function uc_donation_incentives_order_pane_callback($op, $arg1,&$form=null,&$form_state=null) {
  
    switch ($op) {
    case 'view':
      $calculated_incentive = 0;
      $purchased_donation = 0;
      $total_donation = 0;
      if(isset($arg1)) {
        if(isset($arg1->donation_incentive->rebated)) {
          $calculated_incentive = $arg1->donation_incentive->rebated;
          $total_donation += $calculated_incentive;
        }
        if(isset($arg1->donation_incentive->purchased)) {
          $purchased_donation = $arg1->donation_incentive->purchased;
          $total_donation += $purchased_donation;
        }
      }
      $output = '';
      if($total_donation > 0) {
        $output .= t('This order resulted in a total donation of %amount with the following breakdown:', array('%amount' => check_plain(uc_currency_format($total_donation)),));
        $output .= '<ul>';
        if($calculated_incentive > 0) {
          $output .= '<li>'.t('Product Incentives').': '.check_plain(uc_currency_format($calculated_incentive));
        }
        if($purchased_donation > 0) {
          $output .= '<li>'.t('Purchased Donation').': '.check_plain(uc_currency_format($purchased_donation));
        }
        $output .= '</ul>';
      }
      $form['donation_incentives']=array(
        '#type'=>'item',
        '#markup'=>$output
      );
      $output=$form+ drupal_get_form('uc_donation_incentives_update_status_form', $arg1->donation_incentive);
      return $output;
  }
}
// 9597684616
// 8650n
// 011204055