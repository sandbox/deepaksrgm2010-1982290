<?php

/**
 * @file uc_donation_incentive done by s.deepak 011204055 
 */

/**
 * callback for hook_checkout_pane()
 * @param type $op
 * @param type $arg1
 * @param type $arg2
 * @param type $form
 * @param type $form_state
 * @return boolean|string
 */
function uc_donation_incentives_checkout_pane_donations($op, &$arg1, $arg2, $form = null, $form_state = null) {
  switch ($op) {
    case 'view':
      if (isset($arg1) && isset($arg1->donation_incentive->rebated)) {
        $donation_incentive = $arg1->donation_incentive->rebated;
      }
      else {
        $donation_incentive = uc_donation_incentives_calc_cart_incentive();
      }
      $contents['donation_calculated'] = array(
        '#type' => 'value',
        '#value' => $donation_incentive,
      );
      if ($donation_incentive) {
        $contents['donation_incentive'] = array(
          '#value' => theme('uc_donation_incentives_donations_preview', array('subtotal' => $donation_incentive,)),
        );
      }
      else {
        $contents = array();
      }
      $contents['donation_additional'] = array(
        '#title' => t('Top-Up Donation'),
        '#description' => t('You can optionally add an additional donation amount to your purchase (dollar amount).'),
        '#type' => 'textfield',
        '#default_value' => (isset($arg1) && isset($arg1->donation_incentive->purchased)) ? check_plain(uc_currency_format($arg1->donation_incentive->purchased, FALSE)) : '',
      );
      // Js for updating line item.
      drupal_add_js(drupal_get_path('module', 'uc_donation_incentives') . '/uc_donation_incentives.js');
      return array('description' => '<div id="donation-incentive-preview">' . theme('uc_donation_incentives_donations_preview', array('subtotal' => uc_donation_incentives_calc_cart_incentive(),)) . '</div>', 'contents' => $contents,);
      break;

    case 'process':
      if (isset($arg2['panes']['cart_check_out']['donation_additional']['#value']) && $arg2['panes']['cart_check_out']['donation_additional']['#value']) {
        $pattern = '/^\d*(\.\d*)?$/';
        $price_error = t('Additional Donation amount must be in a valid number. No commas and only one decimal point.');
        $donation_addition = $arg2['panes']['cart_check_out']['donation_additional']['#value'];
        if (!is_numeric($donation_addition) && !preg_match($pattern, $donation_addition)) {
          form_set_error('donation_additional', $price_error);
          return FALSE;
        }
        $arg1->donation_incentive->purchased = $donation_addition;
      }
      $arg1->donation_incentive->rebated = isset($arg2['donation_calculated']) ? $arg2['donation_calculated'] : uc_donation_incentives_calc_cart_incentive();
      return TRUE;
      break;

    case 'review':
      $review = array();
      if (uc_donation_incentives_calc_cart_incentive()) {
        $review[] = array(
          'title' => t('Included Donation'), 
          'data' => check_plain(uc_currency_format(uc_donation_incentives_calc_cart_incentive())),
          );
      }
      else {
        $review[] = array(
          'title' => t('Included Donation'), 
          'data' => t('none'),
          );
      }
      if (isset($arg1->donation_incentive->purchased) && $arg1->donation_incentive->purchased) {
        $review[] = array(
          'title' => t('Top-Up Donation'), 
          'data' => check_plain(uc_currency_format($arg1->donation_incentive->purchased)),
          );
      }
      return $review;
      break;

    case 'settings':
      $form['uc_donation_incentives_topup'] = array(
        '#type' => 'checkbox',
        '#title' => t('Allow customers to add an additional donation amount to their order.'),
        '#default_value' => variable_get('uc_donation_incentives_topup', FALSE),
      );
      return $form;
      break;
  }
}

// 011204055
// 9597684616
// 8650n
 