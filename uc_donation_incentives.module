<?php

/**
 * @file uc_donation_incentive done by s.deepak 011204055 
 * Integrates Benevity donations into Ubercart
 */
require_once 'uc_donation_incentives.order_pane.inc';
require_once 'uc_donation_incentives.checkout_pane.inc';
define('UC_DONATION_INCENTIVES_STATUS_PENDING', 'pending');
define('UC_DONATION_INCENTIVES_STATUS_COMPLETE', 'complete');
define('UC_DONATION_INCENTIVES_STATUS_CANCELLED', 'cancelled');

/* * ******************************************************************
 * Drupal Hooks
 * ******************************************************************
 */

/**
 * Implements hook_theme()
 */
function uc_donation_incentives_theme($existing, $type, $theme, $path) {
  return array(
    'uc_donation_incentives_incentive' => array(
      'arguments' => array('donation_incentive' => '', 'teaser' => 0,),
      'template' => 'product-donation-incentive',
    ),
    'uc_donation_incentives_donations_preview' => array(
      'arguments' => array('subtotal' => FALSE,),
    ),
    'uc_donation_incentives_icon' => array(
      'arguments' => array('big' => 0,)
    )
  );
}

/**
 * Implements hook_menu()
 */
function uc_donation_incentives_menu() {
  $items = array();
  $items['admin/store/settings/donation_incentives'] = array(
    'title' => 'Donation Incentive Settings',
    'description' => 'Settings for charitable donations with purchases',
    'access arguments' => array('administer donation incentives',),
    'page callback' => 'uc_donation_incentives_admin',
    'type' => MENU_NORMAL_ITEM,
    'file' => 'uc_donation_incentives.admin.inc',
  );
  $items['admin/store/reports/donation_incentives'] = array(
    'title' => 'Donation Incentives Report',
    'description' => 'Charitable donations with purchases',
    'access arguments' => array('view donation incentives report',),
    'page callback' => 'uc_donation_incentives_report',
    'type' => MENU_NORMAL_ITEM,
    'file' => 'uc_donation_incentives.admin.inc',
  );
  return $items;
}

/**
 * Implements hook_perm().
 */
function uc_donation_incentives_permission() {
  $permission['administer donation incentives'] = array(
    'title' => 'administer donation incentives',
    'description' => 'administer donation incentives',
  );
  $permission['view donation incentives report'] = array(
    'title' => 'view donation incentives report',
    'description' => 'view donation incentives report',
  );
  $permission['edit donation incentives statuses'] = array(
    'title' => 'edit donation incentives statuses',
    'description' => 'edit donation incentives statuses',
  );
  return $permission;
}

/**
 * Implements hook_form_alter().
 * Add package type to products.
 * @see uc_product_form()
 * @see uc_ups_product_alter_validate()
 */
function uc_donation_incentives_form_alter(&$form, &$form_state, $form_id) {
  if (uc_product_is_product_form($form)) {
    $node = $form['#node'];
    $settings = uc_donation_incentives_match_get('product', isset($node->nid) ? $node->nid : 0);
    $form['donation'] = array(
      '#type' => 'fieldset',
      '#title' => t('Donation Incentive'),
      '#collapsible' => TRUE,
      '#tree' => TRUE,
      '#weight' => 0,
      '#description' => t('You can change or disable the donation incentive for this product, or it can inherit the default settings.'),
    );
    $form['donation']['match_type'] = array(
      '#type' => 'select',
      '#title' => t('Donation incentive type'),
      '#description' => t('How would you like to determine the donation incentive a product receives?'),
      '#options' => array(
        'default' => 'use default settings',
        'none' => 'no donation',
        'fixed' => 'fixed amount',
        'sell_fraction' => 'percentage of product sell price',
      ),
      '#default_value' => ($settings ? $settings->match_type : 'default')
    );
    $form['donation']['match_rate'] = array(
      '#type' => 'textfield',
      '#title' => t('Donation incentive rate'),
      '#description' => t('The rate at which a donation is given. For "fixed amount", enter a penny amount (ie: for $1 enter 100). For "percentage" options, enter a value from 0 to 100.'),
      '#default_value' => $settings ? $settings->match_rate : 0,
    );

    $form['#validate'][] = 'uc_donation_incentives_match_validate';
    $form['#submit'][] = 'uc_donation_incentives_match_product_submit';
  }
}

/**
 * implements hook_node_insert
 * @param type $node
 */
function uc_donation_incentives_node_insert($node) {
  $your_required_type = uc_product_types();
  if (in_array($node->type, $your_required_type)) {
    $donation = $node->donation;
    $target_type = 'product';
    $target_id = $node->nid;
    $match_type = $donation['match_type'];
    $match_rate = $donation['match_rate'];
    uc_donation_incentives_settings_save($match_type, $match_rate, $target_type, $target_id);
  }
}

/**
 * Implements hook_node_view
 */
function uc_donation_incentives_node_view($node, $view_mode, $langcode) {
  $weight = variable_get('uc_product_field_weight', array('donation_incentive' => 10,));
  $enabled = variable_get('uc_product_field_enabled', array('donation_incentive' => 0,));
  $node->content['donation_incentive'] = array(
    '#value' => theme('uc_donation_incentives_incentive', array('donation_incentive' => $node->donation_incentive, 'teaser' => 0)),
    '#weight' => $weight['donation_incentive'],
    '#access' => $enabled['donation_incentive'],
  );
}

/**
 * Implements hook_noad_load
 */
function uc_donation_incentives_node_load($nodes, $types) {
  // Decide whether any of $types are relevant to our purposes.
  $your_required_type = uc_product_types();
  foreach ($nodes as $node) {
    if (in_array($node->type, $your_required_type)) {
      $nodes[$node->nid]->donation_incentive = uc_donation_incentives_product_match($node->nid);
      $weight = variable_get('uc_product_field_weight', array('donation_incentive' => 10,));
      $enabled = variable_get('uc_product_field_enabled', array('donation_incentive' => 0,));
    }
  }
}

/**
 * Implements hook_form_FORM_ID_alter()
 * Add custom field to product settings admin
 */
function uc_donation_incentives_form_uc_product_field_settings_form_alter(&$form, $form_state) {
  list($options, $enabled, $weight) = $form['fields']['#summary arguments'];
  $field_label = 'donation_incentive';
  $field = array(
    'title' => 'Donation Incentive',
    'enabled' => $enabled[$field_label],
    'weight' => $weight[$field_label],
  );
  $form['fields'][$field_label]['enabled'] = array('#type' => 'checkbox',
    '#default_value' => $field['enabled'],
  );
  $form['fields'][$field_label]['title'] = array('#type' => 'markup',
    '#value' => $field['title'],
  );
  $form['fields'][$field_label]['weight'] = array('#type' => 'weight',
    '#delta' => 10,
    '#default_value' => $field['weight'],
  );
  // Append our option for summarization.
  $options[$field_label] = $field['title'];
  $form['fields']['#summary arguments'][0] = $options;
}

/**
 * Implements hook_form_FORM_ID_alter()
 * Add a donation indicator to items in the default cart pane form
 */
function uc_donation_incentives_form_uc_cart_view_form_alter(&$form, &$form_state) {
  foreach ($form['items'] as $key => $params) {
    if (!is_array($params))
      continue;
    foreach ($params as $key_1 => $item) {
      if (!isset($item->donation_incentive))
        continue;
      $form['items'][$key]['desc']['#markup'] = $form['items'][$key]['desc']['#markup'] . '<div class="product-donation-incentive-teaser"><a href="javascript:void(0);">' . theme('uc_donation_incentives_incentive', array('donation_incentive' => $item->donation_incentive, 'teaser' => 0)) . '</a></div>';
    }
  }
}

/* * ****************************************************************************
 * Ubercart Hooks
 * **************************************************************************** 
 */

/**
 * Implements hook_uc_cart_pane
 */
function uc_donation_incentives_uc_cart_pane($items) {
  $body = array();
  if (!is_null($items)) {
    if (uc_donation_incentives_calc_cart_incentive()) {
      $body = drupal_get_form('uc_donation_incentives_uc_donation_cart_pane_form') + array(
        '#prefix' => '<div id="cart-form-pane-one">',
        '#suffix' => '</div>',
      );
    }
  }
  $panes['cart_form_donation_incentives'] = array(
    'title' => t('Donation Incentives'),
    'enabled' => TRUE,
    'weight' => 0,
    'body' => $body,
  );
  return $panes;
}

/**
 * Form to be added in add to cart page
 * @param array $form
 * @param type $form_state
 * @return string
 */
function uc_donation_incentives_uc_donation_cart_pane_form($form, $form_state) {
  $form['pane'] = array(
    '#type' => 'item',
    '#markup' => '<div id="donation-incentive-preview">' . theme('uc_donation_incentives_donations_preview', array('subtotal' => uc_donation_incentives_calc_cart_incentive(),)) . '</div>',
  );
  return $form;
}

/**
 * Implements hook_cart_item()
 */
function uc_donation_incentives_cart_item($op, &$item) {
  switch ($op) {
    case 'load':
      $item->donation_incentive = uc_donation_incentives_product_match($item->nid);
      break;
  }
}

/**
 * Implements hook_checkout_pane()
 */
function uc_donation_incentives_uc_checkout_pane() {
  $panes = array();
  $panes['cart_check_out'] = array(
    'callback' => 'uc_donation_incentives_checkout_pane_donations',
    'title' => t('Donations'),
    'desc' => t("Display the contents of a customer's shopping cart."),
    'weight' => 2,
    'process' => FALSE,
    'collapsible' => FALSE,
  );
  return $panes;
}

/**
 * Implements hook_uc_order()
 */
function uc_donation_incentives_uc_order($op, &$arg1, $arg2) {
  switch ($op) {
    case 'new':
      $arg1->donation_incentive = new stdClass();
      $arg1->donation_incentive->rebated = uc_donation_incentives_calc_cart_incentive();
      break;

    case 'save':
      if (isset($arg1->donation_incentive)) {
        $fields = array(
          'deleted_timestamp' => time(), 
          'order_id' => $arg1->order_id,
          );
        db_update("uc_donation_incentives_orders")
            ->fields($fields)
            ->condition('order_id', $arg1->order_id)
            ->execute();
      }
      if ((isset($arg1->donation_incentive->rebated) && $arg1->donation_incentive->rebated) || (isset($arg1->donation_incentive->purchased) && $arg1->donation_incentive->purchased)) {
        $record = array(
          'order_id' => $arg1->order_id,
          'rebated' => isset($arg1->donation_incentive->rebated) ? $arg1->donation_incentive->rebated : 0,
          'purchased' => isset($arg1->donation_incentive->purchased) ? $arg1->donation_incentive->purchased : 0,
          'status' => UC_DONATION_INCENTIVES_STATUS_PENDING,
          'added_timestamp' => time(),
        );
        drupal_write_record('uc_donation_incentives_orders', $record);
      }
      break;

    case 'load':
      $incentive_order = db_select('uc_donation_incentives_orders', 'uc')
          ->fields('uc')
          ->condition('order_id', $arg1->order_id, '=')
          ->condition('deleted_timestamp', 0, '=')
          ->execute()
          ->fetch();
      $arg1->donation_incentive = new stdClass();
      $arg1->donation_incentive = $incentive_order;
      if (is_object($incentive_order)) {
        $arg1->donation_incentive->total = $incentive_order->rebated + $incentive_order->purchased;
      }
      else {
        $arg1->donation_incentive = new stdClass();
        $arg1->donation_incentive->total = 0;
      }
      break;
  }
}

/**
 * Implements hook_order_product_alter();
 */
function uc_donation_incentives_order_product_alter(&$product, $order) {
  $product->donation_incentive->calculated = uc_donation_incentives_calc_item_incentive($product);
  $product->data['donation_incentive'] = $product->donation_incentive;
}

/**
 * Implements hook_order_pane().
 */
function uc_donation_incentives_uc_order_pane() {
  $panes['donation_incentives'] = array(
    'id' => 'donation_incentive',
    'callback' => 'uc_donation_incentives_order_pane_callback',
    'title' => t('Donation'),
    'desc' => t('View donation incentives created for an order.'),
    'weight' => 10,
    'show' => array('view', 'edit'),
  );
  return $panes;
}

/**
 * Implements hook_line_item().
 */
function uc_donation_incentives_uc_line_item() {
  $items[] = array(
    'id' => 'donation_additional',
    'title' => t('Top-Up Donation'),
    'weight' => 2,
    'stored' => false,
    'calculated' => TRUE,
    'display_only' => false,
    'add_list' => false,
    'callback' => 'uc_donation_incentives_donation_line_item_callback',
  );
  return $items;
}

/**
 * Handle the donation line item.
 */
function uc_donation_incentives_donation_line_item_callback($op, $arg1) {
  switch ($op) {
    case 'cart-preview':
      drupal_add_js('
            $(document).ready( function() {
              var donation_additional = $("input[@name*=donation_additional]").val();
              if(donation_additional > 0) {
                set_line_item("donation_additional", "Top-Up Donation", Number(donation_additional).toFixed(2), 1)
              }
            });', 'inline');
      break;

    case 'load':
      $donation = isset($arg1->donation_incentive->purchased) ? $arg1->donation_incentive->purchased : '';
      $lines[] = array(
        'id' => 'donation_additional',
        'title' => t('Top-Up donation'),
        'amount' => $donation,
      );
      return $lines;
  }
}

/* * ****************************************************************************
 * Theming Functions
 * **************************************************************************** */

/**
 * Format the donation incentive summary the cart and checkout pages.
 */
function theme_uc_donation_incentives_donations_preview($subtotal = FALSE) {
  drupal_add_css(drupal_get_path('module', 'uc_donation_incentives') . '/uc_donation_incentives.css', array('type' => 'internal',));
  $output = '';
  if ($subtotal === FALSE)
    $subtotal = uc_donation_incentives_calc_cart_incentive();
  if (variable_get('uc_donation_incentives_topup', FALSE) || $subtotal) {
    $output .= '<div class="donation-incentive-description">' . theme('uc_donation_incentives_icon', 1);
    if ($subtotal) {
      $output .= '<div><b>' . t('Charitable Donation for !causeofchoice: !amount', array('!amount' => check_plain(uc_currency_format(isset($subtotal['subtotal']) ? $subtotal['subtotal'] : '')), '!causeofchoice' => variable_get('uc_donation_incentives_cause', 'your <b>Cause of Choice</b>'))) . '</b></div>';
    }
    $output .= variable_get('uc_donation_incentives_description', 'After you complete the checkout process, you will have the opportunity to select the causes you wish to support.');
    $output .= '</div>';
  }
  return $output;
}

/**
 * Format the donation incentive icon
 * @param type $big
 * @return type
 */
function theme_uc_donation_incentives_icon($big = 0) {
  if ($big) {
    $variables = array(
      'path' => variable_get('uc_donation_incentives_icon_big', drupal_get_path('module', 'uc_donation_incentives') . '/heart32x32.png'),
    );
    $indicator = theme('image', $variables);
  }
  else {
    $variables = array(
      'path' => variable_get('uc_donation_incentives_icon_small', drupal_get_path('module', 'uc_donation_incentives') . '/heart22x22.png'),
    );
    $indicator = theme('image', $variables);
  }
  return $indicator;
}

/**
 * Theme preprocess function
 */
function uc_donation_incentives_preprocess_uc_donation_incentives_incentive(&$variables) {
  drupal_add_css(drupal_get_path('module', 'uc_donation_incentives') . '/uc_donation_incentives.css');
  switch ($variables['donation_incentive']->match_type) {
    default:
    case 'none':
      $variables['product_donation_incentive'] = 0;
      break;
    case 'fixed':
      $variables['product_donation_incentive'] = t('!amount of the purchase price of this item goes to !causeofchoice', array('!amount' => $variables['donation_incentive']->formatted, '!causeofchoice' => variable_get('uc_donation_incentives_cause', 'your <b>Cause of Choice</b>'),));
      break;
    case 'sell_fraction':
      $variables['product_donation_incentive'] = t('!amount of the purchase price of this item goes to !causeofchoice', array('!amount' => $variables['donation_incentive']->formatted, '!causeofchoice' => variable_get('uc_donation_incentives_cause', 'your <b>Cause of Choice</b>'),));
      break;
  }
}

/**
 * uc_catalog doesn't offer hooks for adding fields to products in "grid" view.
 * This function overrides the grid theme function
 */
function phptemplate_uc_catalog_product_grid($products) {
  $product_table = '<div class="category-grid-products"><table>';
  $count = 0;
  foreach ($products as $nid) {
    $product = node_load($nid);

    if ($count == 0) {
      $product_table .= "<tr>";
    }
    elseif ($count % variable_get('uc_catalog_grid_display_width', 3) == 0) {
      $product_table .= "</tr><tr>";
    }

    $titlelink = l($product->title, "node/$nid", array('html' => TRUE,));
    if (module_exists('imagecache') && ($field = variable_get('uc_image_' . $product->type, '')) && isset($product->$field) && file_exists($product->{$field}[0]['filepath'])) {
      $imagelink = l(theme('imagecache', 'product_list', $product->{$field}[0]['filepath'], $product->title, $product->title), "node/$nid", array('html' => TRUE));
    }
    else {
      $imagelink = '';
    }

    $product_table .= '<td>';
    if (variable_get('uc_catalog_grid_display_title', TRUE)) {
      $product_table .= '<span class="catalog-grid-title">' . $titlelink . '</span>';
    }
    if (variable_get('uc_catalog_grid_display_model', TRUE)) {
      $product_table .= '<span class="catalog-grid-ref">' . $product->model . '</span>';
    }
    $product_table .= '<span class="catalog-grid-image">' . $imagelink . '</span>';
    if (variable_get('uc_catalog_grid_display_sell_price', TRUE)) {
      $product_table .= '<span class="catalog-grid-sell-price">' . uc_currency_format($product->sell_price) . '</span>';
    }
    // Add donation incentive indicator.
    $product_table .= theme('uc_donation_incentives_incentive', array('donation_incentive' => $product->donation_incentive, 'teaser' => 1,));
    if (variable_get('uc_catalog_grid_display_add_to_cart', TRUE)) {
      if (variable_get('uc_catalog_grid_display_attributes', TRUE)) {
        $product_table .= theme('uc_product_add_to_cart', $product);
      }
      else {
        $product_table .= drupal_render(drupal_get_form('uc_catalog_buy_it_now_form_' . $product->nid, $product));
      }
    }
    $product_table .= '</td>';
    $count++;
  }
  $product_table .= "</tr></table></div>";
  return $product_table;
}

/* * ****************************************************************************
 * Module and helper functions
 * **************************************************************************** */

/**
 * Form validation handler
 * Validates forms where match settings are set
 */
function uc_donation_incentives_match_validate(&$form, &$form_state) {
  if ($form_state['values']['donation']['match_type'] != 'default' && $form_state['values']['donation']['match_type'] != 'none') {
    if (!is_numeric($form_state['values']['donation']['match_rate'])) {
      form_set_error('donation][match_rate', t('Incentive rate is invalid. Must be a number.'));
    }
    if (!is_int($form_state['values']['donation']['match_rate'] * 1)) {
      form_set_error('donation][match_rate', t('Incentive rate is invalid. Please leave off any punctuation.'));
    }
    if ($form_state['values']['donation']['match_rate'] < 0) {
      form_set_error('donation][match_rate', t('Incentive rate must be positive.'));
    }
    if ($form_state['values']['donation']['match_type'] == 'sell_fraction') {
      if ($form_state['values']['donation']['match_rate'] > 100 || $form_state['values']['donation']['match_rate'] < 0) {
        form_set_error('donation][match_rate', t('Invalid percentage, it should be between 0 and 100.'));
      }
    }
  }
}

/**
 * Form submission handler
 * Saves product-specific donation settings for product nodes
 */
function uc_donation_incentives_match_product_submit(&$form, &$form_state) {
  $target_type = 'product';
  $target_id = $form_state['values']['nid'];
  if ($target_id != 0) {
    $match_type = $form_state['values']['donation']['match_type'];
    $match_rate = $form_state['values']['donation']['match_rate'];
    uc_donation_incentives_settings_save($match_type, $match_rate, $target_type, $target_id);
  }
}

/**
 * Fetches the matching settings for a target type and id
 * @param string $target_type
 * @param int $target_id
 * @return object The match settings
 */
function uc_donation_incentives_match_get($target_type, $target_id = null) {
  $query = db_select('uc_donation_incentives', 'uc_i')
      ->fields('uc_i')
      ->condition('target_type', $target_type, '=');
  $query->condition('target_id', $target_id, '=');
  $result = $query->condition('deleted_timestamp', 0, '=')
      ->execute()
      ->fetch();
  return $result;
}

/**
 * Default global match settings
 * @return object
 */
function uc_donation_incentives_global_match_default() {
  $default = uc_donation_incentives_match_get('default', 0);
  if (!$default) {
    $default = new stdClass();
    $default->target_type = 'default';
    $default->target_id = 0;
    $default->match_type = 'none';
    $default->match_rate = '0';
  }
  return $default;
}

/**
 * Determine the incentive that is applied to a product, or is inherited by a product
 * @param int $product_nid
 */
function uc_donation_incentives_product_match($product_nid) {
  $incentive = uc_donation_incentives_match_get('product', $product_nid);
  if (!$incentive) {
    $incentive = uc_donation_incentives_global_match_default();
  }
  $incentive->formatted = uc_donation_incentives_format_incentive($incentive);
  return $incentive;
}

/**
 * Donation incentive format
 * @param type $incentive
 * @return int
 */
function uc_donation_incentives_format_incentive($incentive) {
  $curr = uc_currency_format($incentive->match_rate / 100); //match_rate is in pennies
  switch ($incentive->match_type) {
    default:
    case 'none':
      return 0;
    case 'fixed':
      return uc_currency_format($incentive->match_rate / 100); //match_rate is in pennies
    case 'sell_fraction':
      return $incentive->match_rate . '%';
  }
}

/**
 * Calculates the incentive for a cart item
 */
function uc_donation_incentives_calc_item_incentive($item) {
  $incentive = $item->donation_incentive;
  $quantity = $item->qty ? $item->qty : 1;
  switch ($incentive->match_type) {
    default:
    case 'none':
      $donation = 0;
      break;
    case 'fixed':
      $donation = ($incentive->match_rate / 100) * $quantity; //match rate will be in pennies
      break;
    case 'sell_fraction':
      $donation = ($incentive->match_rate / 100) * $item->price * $quantity;
      break;
  }
  return uc_currency_format($donation, FALSE);
}

/**
 * Calculates the total donation incentive in a cart
 * (prices in a cart are subject to change)
 * @return float Currency amount
 */
function uc_donation_incentives_calc_cart_incentive() {
  static $subtotal = 0;
  if ($subtotal)
    return $subtotal;
  foreach (uc_cart_get_contents() as $item) {
    $donation = uc_donation_incentives_calc_item_incentive($item);
    $subtotal += $donation;
  }
  return $subtotal;
}

/**
 * Saves donation settings, maintaining an audit trail
 * @param string $match_type
 * @param int $match_rate
 * @param string $target_type
 * @param int $target_id
 */
function uc_donation_incentives_settings_save($match_type, $match_rate, $target_type, $target_id = 0) {
  $existing = uc_donation_incentives_match_get($target_type, $target_id);
  if ($existing) {
    if ($match_type == 'default' || $match_type != $existing->match_type || $match_rate != $existing->match_rate || $target_type != $existing->target_type || $target_id != $existing->target_id) {
      $existing->deleted_timestamp = time();
      drupal_write_record('uc_donation_incentives', $existing, array('id',));
    }
    else {
      return FALSE;
    }
  }
  if ($match_type == 'default') {
    return FALSE;
  }
  $record = array(
    'target_type' => $target_type,
    'target_id' => $target_id,
    'match_type' => $match_type,
    'match_rate' => $match_rate,
    'added_timestamp' => time(),
  );
  drupal_write_record('uc_donation_incentives', $record);
  return TRUE;
}

/**
 * Form builder for updating order donation status
 */
function uc_donation_incentives_update_status_form($form_state, $donation_incentive) {
  $form = array();
  $form['donation_incentive'] = array(
    '#type' => 'value',
    '#value' => $donation_incentive,
  );
  $form['donation'] = array(
    '#theme' => 'uc_order_view_update_controls',
    '#weight' => 10,
  );
  $form['donation']['donationstatus'] = array(
    '#title' => t('Donation status'),
    '#type' => 'select',
    '#options' => array(
      UC_DONATION_INCENTIVES_STATUS_PENDING => 'Pending',
      UC_DONATION_INCENTIVES_STATUS_COMPLETE => 'Complete',
      UC_DONATION_INCENTIVES_STATUS_CANCELLED => 'Cancelled',
    ),
    '#attributes' => array('style' => 'display: inline;',),
    '#default_value' => $donation_incentive->status,
  );
  $form['donation']['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Update',
    '#attributes' => array('style' => 'display: inline;',),
  );
  return $form;
}

/**
 * Submit handler for form handler
 * @param type $form - fapi array
 * @param type $form_state - form state array
 */
function uc_donation_incentives_update_status_form_submit($form, $form_state) {
  $di = $form_state['values']['donation_incentive'];
  uc_donation_incentives_update_status($di->id, $form_state['values']['donationstatus']);
  uc_order_log_changes($di->order_id, array(t('Donation status changed from %orig to %new', array('%orig' => $di->status, '%new' => $form_state['values']['donationstatus'],))));
  drupal_set_message('Donation status has been updated.');
}

/**
 * API donation incentive update status
 */
function uc_donation_incentives_update_status($id, $status) {
  db_update('uc_donation_incentives_orders')
      ->field('status', $status)
      ->condition('id', $id)
      ->condition('deleted_timestamp', 0)
      ->execute();
}
